import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Subiect2 implements ActionListener{

	final String numefisier;
	JButton button1;
	JTextArea tarea;
	Subiect2()
	{
		this.numefisier = "fisier";
		JFrame frame=new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		frame.setSize(400,400);
		frame.setTitle("Asistent Emotional Personal");
		frame.setResizable(false);
		
		
		JPanel panel=new JPanel();
		panel.setLayout(null);
		frame.add(panel);
		
	    button1=new JButton("Scrie");
		button1.setBounds(100, 200, 80, 30);
		panel.add(button1);
		
		JTextArea tarea=new JTextArea();
		tarea.setBounds(100, 100, 50, 50);
		panel.add(tarea);
		
		frame.setVisible(true);
		
		button1.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==button1)
		{
			 
			String copytext=tarea.getText();
			FileWriter fileWriter;
		    try
		    {
		    	fileWriter = new FileWriter(this.numefisier);
		    	fileWriter.write(copytext);
		    }
		    catch(Exception e2)
		    {
		    	System.out.println("FileNotFound");
		    }
		}
			
	}
	public static void main(String[] args) {
		Subiect2 s2=new Subiect2();
	}

}
